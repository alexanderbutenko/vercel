import React from 'react';
import s from './two-col-sublayout.module.scss';

export const TwoColSublayout = (props) => {
    return (
        <div className={s.grid}>
            <div className={s.side}>
                <props.side />
            </div>
            <div className={s.main}>
                {props.children}
            </div>
        </div>
    );
};
