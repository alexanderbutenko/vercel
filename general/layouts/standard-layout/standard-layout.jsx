import React from 'react';
import { Footer } from 'fanx-ui-framework/components/footer';
import { Header } from 'fanx-ui-framework/components/header';

export const StandardLayout = (props) => {
    return (
        <div className='page'>
            <Header />
            <main>
                {props.children}
            </main>
            <Footer />
        </div>
    );
};
