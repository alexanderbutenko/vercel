export interface IGeneralFormResponse {
    success: boolean;
    message?: string;
    payload?: IPayload;
}

export interface IPayload {
    [key: string]: any;
}
