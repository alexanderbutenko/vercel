export interface ICategory {
    id: string;
    parentId: string | null;
    clientId: string;
    slug: string;
    text: string;
    i18N: string | null;
    lastModified: {
        date: string;
    };
    children: [];
}

export interface IArticleContent {
    id: string;
    contentType: string;
    content: string;
    title: string | null;
    image: string | null;
    imageThumbnail: string | null;
    videoThumbnail: string | null;
    publishDate: string | null;
    isHtml: boolean;
    text: null;
    author: null;
    children: null;
    sourceSystemId: null;
    tags: null;
    articleIds: string | null;
    categoryIds: string | null;
    linkedIds: null;
    feedUrl: string | null;
    sponsor: {
        imageUrl: string | null;
        linkUrl: string | null;
    };
    link: string | null;
}

export interface IHeroMedia {
    content: IArticleContent;
    title: string;
    summary: string | null;
}

export interface IAuthor {
    name: string | null;
    title: string | null;
    imageUrl: null;
    socialHandles: [];
}

export interface IArticleMetaData {
    title: string | null;
    description: string | null;
    ogTitle: string | null;
    ogDescription: string | null;
    ogType: string | null;
    ogImageUrl: string | null;
    twitterCard: string | null;
    twitterSite: string | null;
    twitterCreator: string | null;
}

export interface IArticle {
    id: string;
    publishDate: string;
    tags: null;
    categories: ICategory[];
    displayCategory: ICategory | null;
    slug: string;
    singlePage: boolean;
    heroMedia: IHeroMedia;
    linkedIds: [
        {
            sourceSystem: string;
            sourceSystemId: string;
        }
    ];
    readTimeMinutes: number;
    author: IAuthor;
    auth: {
        loginRequired: boolean;
        roles: [];
    };
    language: string;
    blocked: boolean;
    articleMetaData: IArticleMetaData;
}

export interface IArticlePage {
    article: IArticle;
    statusCode: number;
}

export interface INewsFeedProps {
    articles: IArticle[];
    qsParams: IArticlesQsParams;
    metadata: IArticleMetaData;
}

export interface IArticlesQsParams {
    clientId: string;
    page: number;
    size: number;
    sort?: string;
    singlePage?: boolean;
    title?: string;
    language?: string;
    'linkedId.sourceSystem'?: string;
    'linkedId.sourceSystemId'?: string;
    categoryId?: string;
    categorySlug?: string;
    notTags?: string;
}

export interface IArticlesResponse {
    status: string;
    metadata: IArticleMetaData;
    data: {
        articles: IArticle[];
    };
}

export interface IArticleResponse {
    status: string;
    metadata: IArticleMetaData;
    data: {
        article: IArticle;
    };
}
