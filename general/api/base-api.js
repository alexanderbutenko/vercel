import axios from 'axios';

const baseApi = axios.create({
    timeout: 60000,
});

export { baseApi };

export function isHtmlContentType(response) {
    const contentType = response.headers['content-type'];
    if (contentType) {
        return contentType.includes('text/html');
    }

    return false;
}
