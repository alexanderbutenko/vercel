import { sizes, matchStatuses } from 'fanx-ui-framework/general/app/enums';
import app from 'fanx-ui-framework/general/app/app';
import get from 'lodash/get';
import { ArrayHelper } from 'fanx-ui-framework/general/helpers/index';

import { compile } from 'path-to-regexp';
import { parseISO, isValid } from 'date-fns';

export const getTeamConfigPropById = (id, prop) => {
    const foundTeam = app.state.teams.find((team) => team.id === id);

    if (!foundTeam || !foundTeam[prop]) {
        return null;
    }

    return foundTeam[prop];
};

export const getPlayerById = (match, playerId) => {
    if (!match || !match.homeTeam || !match.awayTeam) return null;

    let foundPlayer = match.homeTeam.players?.find((player) => player.id === playerId);

    if (!foundPlayer) {
        foundPlayer = match.awayTeam.players?.find((player) => player.id === playerId);
    }

    return foundPlayer || null;
};

export const validateOptions = (options, propsList = []) => {
    if (!options) throw new Error('You must provide options object');

    const invalidProps = [];

    Object.keys(options).forEach((key) => {
        if (!propsList.contain[key]) {
            invalidProps.push(key);
        }
    });

    if (invalidProps.length) {
        throw new Error('You must provide: ' + invalidProps.join(', '));
    }
};

export const getBemByStatus = (status) => {
    switch (status) {
        case matchStatuses.POST:
            return 'post';
        case matchStatuses.LIVE:
            return 'live';
        case matchStatuses.PRE:
        case matchStatuses.POSTPONED:
        case matchStatuses.ABANDONED:
            return 'pre';
        default:
            return 'live';
    }
};

export const generateUrl = (pattern, params) => {
    if (!pattern) return null;

    const toPath = compile(pattern);

    return toPath(params);
};

export const generateThumbnailSrc = (entryId, params) => {
    if (!entryId) return null;

    const _params = {
        baseUrl: 'https://open.http.mp.streamamg.com',
        width: 1280,
        height: 720,
        ...params
    };

    const wid = get(app.state, 'streamAmgSettings.widgetParams.wid', '');

    return _params.baseUrl +
        `/p/${wid.slice(1)}` +
        `/sp/${wid.slice(1)}00` +
        `/thumbnail/entry_id/${entryId}` +
        `/width/${_params.width}` +
        `/height/${_params.height}`;
};

export const mapCloudVideos = (data) => {
    if (!data) return [];

    let itemData = get(data, 'sections[0].itemData');

    if (!itemData) {
        itemData = get(data, 'itemData') || [];
    }

    return itemData;
};

export const mapCloudVideo = (cloudVideo) => ({
    entryId: get(cloudVideo, 'mediaData.entryId') || null,
    title: get(cloudVideo, 'metaData.title') || null,
    thumbnailUrl: get(cloudVideo, 'mediaData.thumbnailUrl') || null,
    label: get(cloudVideo, 'metaData.label') || null,
    duration: get(cloudVideo, 'metaData.VideoDuration') || null,
    sysEntryEntitlements: get(cloudVideo, 'metaData.SysEntryEntitlements') || null,
    lastUpdated: get(cloudVideo, 'publicationData.updatedAt') || null,
    createdAt: get(cloudVideo, 'publicationData.createdAt') || null,
    releasedFrom: get(cloudVideo, 'publicationData.createdAt') || null,
    teams: get(cloudVideo, 'publicationData.team') || [],
});

export const getPromoId = (type) => {
    const promoBlocks = app.state.promoBlocks;

    if (!type || !Array.isArray(promoBlocks)) return null;

    const foundPromoBlock = promoBlocks.find((promoBlock) => promoBlock.id === type);

    return foundPromoBlock ? foundPromoBlock.promoId : null;
};

export const getSelectedChoiceValue = (choices) => {
    if (!Array.isArray(choices)) return null;

    const foundChoice = choices.find((choice) => choice.selected);

    return foundChoice ? foundChoice.value : null;
};

export const setSelectedChoice = (choices, selectedValue) => {
    if (ArrayHelper.isEmpty(choices) || selectedValue === undefined) {
        return choices;
    }

    return choices.map((choice) => ({
        ...choice,
        selected: choice.value === selectedValue
    }));
};

export const getPlayerImageOrDefault = (source) => {
    if (!source) {
        return 'f90119a1-9644-4df7-91c0-2c5bd5878d18.png';
    }

    return source;
};

export const getPlayerName = (player) => {
    const playerName = {
        firstName: null,
        lastName: null,
    };

    if (!player) return playerName;

    if ((player.firstName && player.lastName) || typeof player.name !== 'string') return player;

    const splittedName = player.name.split(' ');

    if (ArrayHelper.isEmpty(splittedName)) return player;

    return {
        firstName: splittedName[0],
        lastName: splittedName.slice(1).join(' '),
    };
};

export const sortVideosByReleaseFrom = (a, b) => {
    if (!a || !b) return false;

    const releaseFromA = get(a, 'publicationData.releaseFrom');
    const releaseFromB = get(b, 'publicationData.releaseFrom');

    if (!isValid(parseISO(releaseFromA)) ||
        !isValid(parseISO(releaseFromB))
    ) {
        return false;
    }

    return new Date(releaseFromB) - new Date(releaseFromA);
};

export const excludePaidVideos = (item) => {
    if (!item) return false;

    const sysEntryEntitlements = get(item, 'metaData.SysEntryEntitlements');

    if (Array.isArray(sysEntryEntitlements)) {
        return !sysEntryEntitlements.some((sysEntry) =>
            sysEntry.toLowerCase() === 'paid');
    }

    return true;
};

export const isMatchStatusPre = (status) => status === matchStatuses.PRE ||
        status === matchStatuses.POSTPONED ||
        status === matchStatuses.ABANDONED;

export const isMatchStatusLive = (status) => !(isMatchStatusPre(status) || status === matchStatuses.POST);

export const generateBemCss = (bemName, bemList) => {
    let bemCss = '';

    if (Array.isArray(bemList)) {
        bemCss = bemList
            .filter((bemModifier) => bemModifier)
            .map((bemModifier) => `${bemName}--${bemModifier}`).join(' ');
    }

    return bemCss ? `${bemName} ${bemCss}` : bemName;
};

export const generateBemCssModule = (bemName, bemList, styles) => generateBemCss(bemName, bemList)
    .split(' ')
    .map((css) => styles[css])
    .join(' ');

export const generateModuleName = (rootClass, styles, bemList) => bemList.map((css) => {
    const bemCss = (css.replace('-', ' ')
        .replace(/(^\w)|(\s\w)/g, (match) => match.toUpperCase())
        .replace(' ', ''));
    return styles[rootClass + bemCss];
}).join(' ');

export const getBemList = (propsBemList, stateBemList) => {
    let bemList = [];

    if (Array.isArray(propsBemList)) {
        bemList = [...propsBemList];
    }

    if (Array.isArray(stateBemList)) {
        bemList = [...bemList, ...stateBemList];
    }

    return bemList;
};

export const getCategoryString = (string) => {
    if (!string) return '';

    return string.toLocaleLowerCase().replace(/(\s.\s|\s)/, '-');
};

// https://github.com/facebook/react/issues/5465
export const makeCancelable = (promise) => {
    let hasCanceled_ = false;

    const wrappedPromise = new Promise((resolve, reject) => {
        promise.then(
            (val) => (hasCanceled_ ? reject({ isCanceled: true }) : resolve(val)),
            (error) => (hasCanceled_ ? reject({ isCanceled: true }) : reject(error))
        );
    });

    return {
        promise: wrappedPromise,
        cancel() {
            hasCanceled_ = true;
        },
    };
};

export const getScrollTop = () => document.documentElement.scrollTop || document.body.scrollTop;

export const setScrollTop = (value) => {
    if (typeof value !== 'number') return;

    document.body.scrollTop = value;
    document.documentElement.scrollTop = value;
};

export const getData = async (url) => {
    const response = await fetch(url);
    return response.json();
};

export const getSrcSetStreamAmg = (image) => {
    const regExp = image.match(/(width\/\d*\/height\/\d*)/g).toString();

    const getSizes = (size) => image.replace(regExp, `width/${size}`);

    const srcString = [];

    sizes.forEach((size) => {
        srcString.push(`${getSizes(size)} ${size}w`);
    });

    return srcString.join(', ');
};
