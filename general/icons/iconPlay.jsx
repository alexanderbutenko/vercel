import React from 'react';

const IconPlay = ({
    className, width, height, fill
}) => (
    <svg className={className} fill={fill} version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        width={width} height={height} viewBox="0 0 7 8"
        xmlSpace="preserve">

        <path d="M7,4L0,8V0L7,4z"/>
    </svg>
);

export default IconPlay;
