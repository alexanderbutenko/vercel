import axios from 'axios';

export const axiosNode = axios.create({
    timeout: 600000
});
