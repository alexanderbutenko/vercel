import { createStore, createEvent } from 'effector';
import Router from 'next/router';
import { IAuthProvider } from './auth-providers';
import TYPES from 'dependency-injection/types';
import 'reflect-metadata';
import container from 'dependency-injection/inversify-container';
import { IGeneralFormResponse } from '../../../interfaces/form-interfaces';

export interface IRegistrationParams {
    username: string;
    password: string;
}

export interface IMarketingPreferencesParam {
    [key: string]: string;
}

export interface IProfileParameters {
    [key: string]: string;
}

export interface IMarketingPreferencesMultipleParam {
    key: string;
    preferences: IMarketingPreferencesParam;
}

export interface IProfileResponse extends IGeneralFormResponse {
    payload?: IProfile;
}

export interface IProfile {
    address1: string | null;
    address2: string | null;
    biography: string | null;
    birthDate: string | null;
    contactNumber: string | null;
    country: string | null;
    email: string | null;
    firstName: string | null;
    gender: string | null;
    language: string | null;
    lastName: string | null;
    postcode: string | null;
    town: string | null;
}

const loginEvent = createEvent();
const logoutEvent = createEvent();

const authStore = createStore({
    isLoggedIn: false,
})
    .on(loginEvent, (state) => {
        return {
            ...state,
            isLoggedIn: true,
        };
    })
    .on(logoutEvent, (state) => {
        return {
            ...state,
            isLoggedIn: false,
        };
    });

class Authentication {
    isLoggedIn: boolean;

    private authProvider: IAuthProvider;

    constructor() {
        this.authProvider = container.get<IAuthProvider>(TYPES.IAuthProvider);
        this.isLoggedIn = false;
        this.authProvider.init({
            onLogin: this.onLogin,
            onLogout: this.onLogout,
        });
    }

    getLoginState() {
        return this.authProvider.getLoginState();
    }

    onLogin = () => {
        loginEvent();
    };

    async register(registrationParams: IRegistrationParams) {
        return this.authProvider.register(registrationParams);
    }

    async login(params: IRegistrationParams) {
        return this.authProvider.login(params);
    }

    logout(path = '/') {
        this.onLogout(path);
    }

    onLogout = (path: string) => {
        Router.push(path);
        this.authProvider.logout();
        logoutEvent();
    };

    async getProfile() {
        return this.authProvider.getProfile();
    }

    async getMarketingPreferences() {
        return this.authProvider.getMarketingPreferences();
    }

    async updateMarketingPreferences(preferencesArr: IMarketingPreferencesMultipleParam[]) {
        return this.authProvider.updateMarketingPreferences(preferencesArr);
    }

    async updateProfile(profileData: IProfileParameters) {
        return this.authProvider.updateProfile(profileData);
    }
}

const authentication = new Authentication();

export { authentication, authStore };
