import { IGeneralFormResponse } from 'fanx-ui-framework/general/interfaces/form-interfaces';
import {
    IMarketingPreferencesMultipleParam,
    IRegistrationParams,
    IProfileParameters,
    IProfileResponse,
} from '../authentication';

export interface IAuthProvider {
    init(callbacks: IAuthProviderCallbacks): void;
    getLoginState(): boolean;
    register(params: IRegistrationParams): Promise<IGeneralFormResponse>;
    login(params: IRegistrationParams): Promise<IGeneralFormResponse>;
    logout(): void;
    getProfile(): Promise<IProfileResponse>;
    updateProfile(params: IProfileParameters): Promise<IProfileResponse>;
    getMarketingPreferences(): Promise<IGeneralFormResponse>;
    updateMarketingPreferences(
        preferencesArr: IMarketingPreferencesMultipleParam[]
    ): Promise<IGeneralFormResponse>;
}

export interface IAuthProviderCallbacks {
    onLogin(): void;
    onLogout(path?: string): void;
}
