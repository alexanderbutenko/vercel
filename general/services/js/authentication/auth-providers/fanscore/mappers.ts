import { IFanscoreProfileResponse } from './interfaces';
import { IProfile } from '../../authentication';

export const mapProfileResponse = (response: IFanscoreProfileResponse): IProfile => {
    return {
        address1: response.data.address1,
        address2: response.data.address2,
        biography: response.data.biography,
        birthDate: response.data.birthDate,
        contactNumber: response.data.contactNumber,
        country: response.data.country,
        email: response.data.email,
        firstName: response.data.firstName,
        gender: response.data.gender,
        language: response.data.language,
        lastName: response.data.lastName,
        postcode: response.data.postcode,
        town: response.data.town,
    };
};
