export interface IFanscoreGeneralResponse {
    status: string;
    data: any;
    metadata: {
        createdAt: string;
    };
}

export interface IFanscoreProfileResponse extends IFanscoreGeneralResponse {
    data: IFanscoreProfile;
}

export interface IFanscoreProfile {
    address1: string | null;
    address2: string | null;
    biography: string | null;
    birthDate: string | null;
    contactNumber: string | null;
    country: string | null;
    email: string | null;
    firstName: string | null;
    gender: string | null;
    id: number;
    language: string | null;
    lastName: string | null;
    lastUpdated: string | null;
    otherNames: string | null;
    postcode: string | null;
    profilePicture: string | null;
    profilePictureApproved: boolean;
    region: string | null;
    screenName: string | null;
    screenNameApproved: boolean;
    town: string | null;
}
