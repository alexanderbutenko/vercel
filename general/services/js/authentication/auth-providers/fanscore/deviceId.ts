import { v4 as uuidv4 } from 'uuid';

// let deviceId: string;
// if (process.browser && !deviceId) {
//     const storageClientId = window?.localStorage.getItem('deviceId');
//     if (storageClientId) {
//         deviceId = storageClientId;
//     } else {
//         deviceId = uuidv4();
//         window.localStorage.setItem('deviceId', deviceId);
//     }
// }

class DeviceId {
    id: string;

    constructor() {
        this.id = '';
        console.log('DeviceId constructor');
        if (process.browser) {
            const storageClientId = window?.localStorage.getItem('deviceId');
            if (storageClientId) {
                this.id = storageClientId;
            } else {
                this.id = uuidv4();
                window.localStorage.setItem('deviceId', this.id);
            }
        }
    }

    getId() {
        return this.id;
    }
}

const deviceIdInstance = new DeviceId();

export const deviceId = deviceIdInstance.getId();
