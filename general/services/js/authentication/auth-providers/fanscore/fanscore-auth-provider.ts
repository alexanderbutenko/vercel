import axios, { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import qs from 'qs';
import { IGeneralFormResponse } from 'fanx-ui-framework/general/interfaces/form-interfaces';
import { injectable } from 'inversify';
import { IAuthProvider, IAuthProviderCallbacks } from '../index';
import { endpoints } from './endpoints';
import { clientId } from './constants';
import { deviceId } from './deviceId';
import {
    IRegistrationParams,
    IMarketingPreferencesParam,
    IMarketingPreferencesMultipleParam,
    IProfileParameters, IProfileResponse,
} from '../../authentication';
import { mapProfileResponse } from './mappers';
import 'reflect-metadata';
import { IFanscoreProfileResponse } from './interfaces';

@injectable()
export class FanscoreAuthProvider implements IAuthProvider {
    client: AxiosInstance;

    refreshRequest: any;

    token: string | null;

    refreshToken: string | null;

    isLoggedIn: boolean;

    callbacks: IAuthProviderCallbacks | null;

    constructor() {
        this.token = null;
        this.refreshToken = null;
        this.isLoggedIn = false;
        this.callbacks = null;

        this.client = axios.create({
            headers: { 'Content-Type': 'application/json' },
        });

        this.refreshRequest = null;

        this.client.interceptors.request.use(
            (config: AxiosRequestConfig) => {
                if (!this.token) {
                    return config;
                }

                const newConfig = {
                    headers: {},
                    ...config,
                };

                newConfig.headers.Authorization = `Bearer ${this.token}`;
                return newConfig;
            },
            (e) => Promise.reject(e)
        );

        this.client.interceptors.response.use(
            (r: AxiosResponse) => r,
            async (error: AxiosError) => {
                if (
                    !this.refreshToken ||
                    error.response?.status !== 401 ||
                    // @ts-ignore
                    error.config.retry
                ) {
                    const status = error?.response?.data?.status;

                    if (status === 'fail') {
                        this.callbacks?.onLogout('/profile/sign-in');
                    }
                    throw error;
                }

                if (!this.refreshRequest) {
                    this.refreshRequest = axios({
                        method: 'post',
                        url: endpoints.token,
                        data: qs.stringify({
                            grant_type: 'refresh_token',
                            client_id: clientId,
                            device_id: deviceId,
                            refresh_token: this.refreshToken,
                        }),
                        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    });
                }
                const { data } = await this.refreshRequest;
                console.log('refreshRequest');
                console.log(data);

                this.onLogin(data.access_token, data.refresh_token);
                const newRequest = {
                    ...error.config,
                    retry: true,
                };

                return this.client(newRequest);
            }
        );
    }

    init(callbacks: IAuthProviderCallbacks) {
        this.callbacks = callbacks;
        this.getAuthDataFromLocalStorage();
    }

    getAuthDataFromLocalStorage(): void {
        if (!process.browser) return;
        const authString = window.localStorage.getItem('auth');
        if (authString) {
            try {
                const auth = JSON.parse(authString);
                this.token = auth.token;
                this.refreshToken = auth.refreshToken;
                this.isLoggedIn = auth.isLoggedIn;
                this.callbacks?.onLogin();
            } catch (e) {
                console.log(e);
            }
        }
    }

    getLoginState() {
        return this.isLoggedIn;
    }

    onLogin(token: string, refreshToken: string) {
        this.token = token;
        this.refreshToken = refreshToken;
        this.isLoggedIn = true;
        window.localStorage.setItem(
            'auth',
            JSON.stringify({
                token: this.token,
                refreshToken: this.refreshToken,
                isLoggedIn: true,
            })
        );
        if (this.callbacks && typeof this.callbacks.onLogin === 'function') {
            this.callbacks.onLogin();
        }
    }

    async register({ username, password }: IRegistrationParams) {
        let result: IGeneralFormResponse = {
            success: false,
        };
        await axios({
            method: 'post',
            url: endpoints.token,
            data: qs.stringify({
                username,
                password,
                grant_type: 'register',
                client_id: clientId,
                device_id: deviceId,
            }),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        })
            .then(({ data }) => {
                this.onLogin(data.access_token, data.refresh_token);
                result = {
                    success: true,
                };
            })
            .catch((error) => {
                const message = error?.response?.data?.message;
                result = {
                    success: false,
                    message,
                };
            });

        return result;
    }

    async login({ username, password }: IRegistrationParams) {
        let result: IGeneralFormResponse = {
            success: false,
        };
        await axios({
            method: 'post',
            url: endpoints.token,
            data: qs.stringify({
                username,
                password,
                grant_type: 'password',
                client_id: clientId,
                device_id: deviceId,
            }),
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        })
            .then(({ data }) => {
                this.onLogin(data.access_token, data.refresh_token);
                result = {
                    success: true,
                };
            })
            .catch((error) => {
                const message = error?.response?.data?.message;
                result = {
                    success: false,
                    message,
                };
            });

        return result;
    }

    logout(): void {
        this.token = null;
        this.refreshToken = null;
        this.isLoggedIn = false;
        window.localStorage.removeItem('auth');
    }

    async getProfile() {
        let result: IProfileResponse = {
            success: false,
        };
        await this.client
            .get(endpoints.profile)
            .then((resp: AxiosResponse<IFanscoreProfileResponse>) => {
                result = {
                    success: true,
                    payload: mapProfileResponse(resp.data),
                };
            })
            .catch((error) => {
                const message = error?.response?.data?.message;
                result = {
                    success: false,
                    message,
                };
            });
        return result;
    }

    async getMarketingPreferences() {
        let result: IGeneralFormResponse = {
            success: false,
        };
        await this.client
            .get(endpoints.marketingPreferences, {
                params: {
                    clientId,
                },
            })
            .then(({ data }) => {
                result = {
                    success: true,
                    payload: data,
                };
            })
            .catch((error) => {
                const message = error?.response?.data?.message;
                result = {
                    success: false,
                    message,
                };
            });
        return result;
    }

    async updateSingleMarketingPreferences(
        key: string,
        preferencesObj: IMarketingPreferencesParam
    ) {
        let result: IGeneralFormResponse = {
            success: false,
        };
        await this.client
            .post(endpoints.marketingPreferences, JSON.stringify(preferencesObj), {
                params: {
                    clientId,
                    key,
                },
            })
            .then(({ data }) => {
                result = {
                    success: true,
                    payload: data.data,
                };
            })
            .catch((error) => {
                const message = error?.response?.data?.message;
                result = {
                    success: false,
                    message,
                };
            });
        return result;
    }

    async updateMarketingPreferences(preferencesArr: IMarketingPreferencesMultipleParam[]) {
        const requests = preferencesArr.map((item) => {
            return this.updateSingleMarketingPreferences(item.key, item.preferences);
        });
        let result: IGeneralFormResponse = {
            success: false,
        };

        await Promise.all(requests)
            .then((results: IGeneralFormResponse[]) => {
                result = {
                    success: !results.some((r: IGeneralFormResponse) => !r.success),
                    payload: { preferences: results.map((r) => r.payload) },
                };
            })
            .catch((error) => {
                console.log(error);
                result = {
                    success: false,
                };
            });
        return result;
    }

    async updateProfile(profileData: IProfileParameters) {
        let result: IProfileResponse = {
            success: false,
        };
        await this.client
            .patch(endpoints.profile, profileData)
            .then((resp: AxiosResponse<IFanscoreProfileResponse>) => {
                result = {
                    success: true,
                    payload: mapProfileResponse(resp.data),
                };
            })
            .catch((error) => {
                const message = error?.response?.data?.message;
                result = {
                    success: false,
                    message,
                };
            });
        return result;
    }
}

const fanscoreAuthProvider = new FanscoreAuthProvider();

export { fanscoreAuthProvider };
