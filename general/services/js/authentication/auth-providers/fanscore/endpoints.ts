export enum endpoints {
    token = 'https://oauth-stage.fanscore.com/oauth/token',
    profile = 'https://profile-stage.fanscore.com/v1/profile/me',
    marketingPreferences = 'https://profile-stage.fanscore.com/v1/contactpreferences',
}
