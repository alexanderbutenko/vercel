import FetchService from './js/fetch-service';
import gtmService from './js/gtm';
import { axiosNode } from './js/axios-node';
import { authentication, authStore } from './js/authentication/authentication';

export {
    FetchService,
    gtmService,
    axiosNode,
    authentication,
    authStore
};
