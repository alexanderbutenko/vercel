export enum Endpoints {
    Articles = 'https://article-cms-api.incrowdsports.com/v2/articles',
    ArticleBySlug = 'https://article-cms-api.incrowdsports.com/v2/articles/slug',
    IncrowdSports = 'https://media-cdn.incrowdsports.com',
}
