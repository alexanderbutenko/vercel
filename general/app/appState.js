import appSettings from 'fanx-ui-framework/general/app/appsettings.json';

export const appState = Object.freeze({
    mediaConfig: appSettings.MediaConfig,
    streamAmgSettings: appSettings.StreamAmgSettings,
});
