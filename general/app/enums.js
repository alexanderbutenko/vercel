export const matchStatuses = Object.freeze({
    PRE: 'fixture',
    POST: 'result',
    POSTPONED: 'postponed',
    ABANDONED: 'abandoned',
    HALFTIME: 'halftime',
    FULLTIME: 'result',
});

export const rowNames = {
    TWO_THIRDS_AND_ONE_THIRD: 'TWO_THIRDS_AND_ONE_THIRD',
    TRIPLE_ONE_THIRD: 'TRIPLE_ONE_THIRD',
    TALL: 'TALL',
};

export const rowPattern = {
    [rowNames.TALL]: {
        modifiers: ['tall'],
        slots: [
            {
                type: 'tall',
                modifiers: ['tall']
            }
        ]
    },
    [rowNames.TWO_THIRDS_AND_ONE_THIRD]: {
        modifiers: ['two-thirds-and-one-third'],
        slots: [
            {
                type: 'two-thirds',
                modifiers: ['two-thirds']
            },
            {
                type: 'one-third',
                modifiers: ['one-third']
            }
        ]
    },
    [rowNames.TRIPLE_ONE_THIRD]: {
        modifiers: ['triple-one-third'],
        slots: [
            {
                type: 'one-third',
                modifiers: ['one-third']
            },
            {
                type: 'one-third',
                modifiers: ['one-third']
            },
            {
                type: 'one-third',
                modifiers: ['one-third']
            }
        ]
    }
};

export const statsValueTypes = Object.freeze({
    SIMPLE: 'simple',
    MIN_MAX: 'minMax',
    PERCENT: 'percent'
});

export const postTypes = Object.freeze({
    TEXT: 'text',
    QUOTE: 'quote',
    IMAGE: 'image',
    VIDEO: 'video',
    ACTION: 'action',
    SOCIAL: 'social',
    TIME: 'time',
    REPLACEMENT: 'replacement'
});

export const actionPostTypes = Object.freeze({
    TRY: 'try',
    CONVERSION: 'conversion',
    PENALTY: 'penalty',
    PENALTY_KICK: 'penaltyKick',
    DROP_GOAL: 'dropGoal',
    RED_CARD: 'redCard',
    YELLOW_CARD: 'yellowCard',
    SUBSTITUTION: 'substitution',
    KICK_OFF: 'kickOff',
    HALF_TIME: 'halfTime',
    FULL_TIME: 'fullTime',
    EXTRA_TIME: 'extraTime',
});

export const socialPostTypes = Object.freeze({
    TWITTER: 'twitter',
    INSTAGRAM: 'instagram'
});

export const contentTypes = Object.freeze({
    TEXT: 'TEXT',
    QUOTE: 'QUOTE',
    IMAGE: 'IMAGE',
    GALLERY: 'GALLERY',
    VIDEO: 'VIDEO',
    PROMO: 'PROMO',
    CLOUD_MATRIX: 'CLOUD_MATRIX',
    RELATED_CONTENT: 'RELATED_CONTENT',
});

export const gameCardTypes = Object.freeze({
    PRE: 'pre',
    LIVE: 'live',
    POST: 'post'
});

export const eventsGroups = [
    {
        name: 'Tries',
        type: 'try'
    },
    {
        name: 'Conversions',
        type: 'conversion'
    },
    {
        name: 'Penalties',
        type: 'penalty',
    },
    {
        name: 'Drop goals',
        type: 'dropGoal'
    },
];

export const sizes = [
    320,
    360,
    640,
    720,
    960,
    1280,
    1440,
    1920
];
