import { appState } from 'fanx-ui-framework/general/app/appState';
import detectHover from 'detect-hover';
import get from 'lodash/get';

class App {
    constructor() {
        // this.config = window.appConfig || {};
        this.state = appState || {};
    }

    init() {
        if (!detectHover.anyHover) {
            const htmlNode = document.querySelector('html');

            if (htmlNode) {
                htmlNode.classList.remove('can-hover');
            }
        }
    }

    getClientId() {
        const clientId = get(this.state, 'sso.clientId');

        if (!clientId) {
            // throw new Error('You must provie clientId');
        }

        return clientId;
    }
}

const app = new App();
export default app;
