Put *.**woff**, *.**woff2** files inside this folder.

A filename should follow kebab-case like: **opensans-semibold.woff**

#Attention, fonts have bug with line-height, use `Font Forge` to fix it manually for new fonts.

