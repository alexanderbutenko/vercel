import React from 'react';
import Link from 'next/link';
import { StandardLayout } from 'fanx-ui-framework/general/layouts/standard-layout';

const IndexPage = (): JSX.Element => {
    return (
        <StandardLayout>
            <Link href={'/latest/'}>
                <a>Latest</a>
            </Link>
        </StandardLayout>
    );
};

export default IndexPage;
