import React from 'react';
import { StandardLayout } from 'fanx-ui-framework/general/layouts/standard-layout/index';
import { SigninForm } from 'fanx-ui-framework/features/profile/components/signin-form/index';
import { ProfileRedirectWrap } from 'fanx-ui-framework/features/profile/components/profile-redirect-wrap/jsx/profile-redirect-wrap';

const SignIn = (): JSX.Element => {
    return (
        <StandardLayout>
            <ProfileRedirectWrap>
                <div className="container side-gaps initial-mt-20 initial-mb-20">
                    <h1 className="mb-10">Sign in</h1>
                    <SigninForm />
                </div>
            </ProfileRedirectWrap>
        </StandardLayout>
    );
};

export default SignIn;
