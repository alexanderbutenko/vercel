import React from 'react';
import NoSSR from 'react-no-ssr';
import { PrivateSectionWrap } from 'fanx-ui-framework/features/profile/components/private-section-wrap/jsx/private-section-wrap';

import { StandardLayout } from 'fanx-ui-framework/general/layouts/standard-layout/index';
import { TwoColSublayout } from 'fanx-ui-framework/general/layouts/two-col-sublayout';
import { ProfileSide } from 'fanx-ui-framework/features/profile/components/profile-form/';
import { PreferencesMain } from 'fanx-ui-framework/features/profile/components/profile-preferences/';

const Preferences = (): JSX.Element => {
    return (
        <StandardLayout>
            <PrivateSectionWrap>
                <div className="container side-gaps initial-mt-20 initial-mb-20">
                    <NoSSR>
                        <TwoColSublayout side={ProfileSide}>
                            <h1 className="mb-10">Preferences</h1>
                            <PreferencesMain />
                        </TwoColSublayout>
                    </NoSSR>
                </div>
            </PrivateSectionWrap>
        </StandardLayout>
    );
};

export default Preferences;
