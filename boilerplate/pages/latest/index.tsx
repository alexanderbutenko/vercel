import React from 'react';
import Head from 'next/head';
import { GetStaticProps } from 'next';
import { AxiosResponse } from 'axios';
import { Endpoints } from 'fanx-ui-framework/general/app/endpoints';
import { NewsFeed } from 'fanx-ui-framework/components/news-feed';
import { axiosNode } from 'fanx-ui-framework/general/services/js/axios-node';
import { StandardLayout } from 'fanx-ui-framework/general/layouts/standard-layout';

import {
    IArticlesResponse,
    IArticlesQsParams,
    INewsFeedProps,
} from 'fanx-ui-framework/general/interfaces/articleIntrfaces';

interface IGetStaticProps {
    props: INewsFeedProps;

    revalidate?: number;
}

const qsParams: IArticlesQsParams = {
    page: 0,
    size: 5,
    sort: '-publishDate',
    categoryId: '94f1c1eb-9434-47c3-99a7-4dd521020013',
    clientId: 'PRO14',
};

const NewsFeedPage = (props: INewsFeedProps): JSX.Element => {
    return (
        <>
            <Head>
                <title>Next.js - News Feed</title>
            </Head>
            <StandardLayout>
                <NewsFeed {...props} />
            </StandardLayout>
        </>
    );
};

export const getStaticProps: GetStaticProps = async (): Promise<IGetStaticProps> => {
    const response: AxiosResponse<IArticlesResponse> = await axiosNode.get(Endpoints.Articles, {
        params: qsParams,
    });
    const { data, metadata } = response.data;

    return {
        props: {
            articles: data.articles,
            qsParams,
            metadata,
        },

        revalidate: 60,
    };
};

export default NewsFeedPage;
