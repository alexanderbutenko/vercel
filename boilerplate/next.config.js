module.exports = {
    sassOptions: {
        prependData: '@import \'general/scss/settings/index.scss\';',
    },
    target: 'serverless',
};
