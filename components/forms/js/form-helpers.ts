import { Dispatch } from 'react';

import { IGeneralFormResponse } from '../../../general/interfaces/form-interfaces';

export const handleFormResponse = (
    response: IGeneralFormResponse,
    setFormErrorMessage: Dispatch<React.SetStateAction<string>>
) => {
    console.log(response);
    setFormErrorMessage('');
    if (response.success) {
        setFormErrorMessage('');
    } else if (response.message) {
        setFormErrorMessage(response.message);
    }
};
