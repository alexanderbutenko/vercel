import { rowNames, rowPattern } from 'fanx-ui-framework/general/app/enums';

export const getPattern = () => [
    rowPattern[rowNames.TALL],
    rowPattern[rowNames.TRIPLE_ONE_THIRD],
];
