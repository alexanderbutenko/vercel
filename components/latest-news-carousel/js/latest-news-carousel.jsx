import React from 'react';
import PropTypes from 'prop-types';
import { ArrayHelper } from 'fanx-ui-framework/general/helpers';
import { NewsCard } from 'fanx-ui-framework/components/news-card';
import { Section } from 'fanx-ui-framework/components/section';
import { Carousel } from 'fanx-ui-framework/components/carousel';

class LatestNewsCarousel extends React.Component {
    render() {
        const {
            bemList,
            allLatestUrl,
            articles,
            collapseToCard,
            ...rest
        } = this.props;

        if (ArrayHelper.isEmpty(articles)) return null;

        if (collapseToCard && articles.length === 1) {
            return (
                <div className="container">
                    <NewsCard article={articles[0]} bemList={['tall']} />
                </div>
            );
        }

        const topArticles = articles.slice(0, 10).map((article) =>
            <NewsCard key={article.id} article={article} />);

        return (
            <Section bemList={bemList}>
                {
                    topArticles.length > 0 &&
                    <Carousel
                        {...rest}
                        slides={topArticles}
                        bemList={bemList}
                        targetUrl={allLatestUrl}
                        targetUrlText={'All latest'}
                    />
                }
            </Section>
        );
    }
}

LatestNewsCarousel.propTypes = {
    title: PropTypes.string,
    bemList: PropTypes.array,
    allLatestUrl: PropTypes.string,
    articles: PropTypes.array,
    sponsorImage: PropTypes.string,
    collapseToCard: PropTypes.bool
};

export default LatestNewsCarousel;
