import React from 'react';
import { ArrayHelper } from 'fanx-ui-framework/general/helpers';
import { generateBemCssModule, getBemList } from 'fanx-ui-framework/general/helpers/helpers';
import { contentTypes } from 'fanx-ui-framework/general/app/enums';
import { twitterLoader } from 'fanx-ui-framework/components/loaders';
import { Richtext } from 'fanx-ui-framework/components/richtext';
import { Quote } from 'fanx-ui-framework/components/quote';
import { Image } from 'fanx-ui-framework/components/image';
import { Gallery } from 'fanx-ui-framework/components/gallery';
import { Video } from 'fanx-ui-framework/components/video';
// import { VideoCarousel } from 'fanx-ui-framework/components/video-carousel';
// import { LatestNewsCarousel } from 'fanx-ui-framework/components/latest-news-carousel';
import styles from '../scss/article-content.module.scss';

class ArticleContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            twitterHasLoaded: false,
        };
        this.rootRef = React.createRef();
        this.unsubscribeOnLoad = () => {
        };
    }

    componentDidMount() {
        this.unsubscribeOnLoad = twitterLoader.subscribeOnLoad(
            () => this.setState({ twitterHasLoaded: true })
        );
        window.addEventListener('load', this.handleLoad);
    }

    handleLoad() {
        const a = document.querySelectorAll('.richtext a');
        if (a.length > 0) {
            a.forEach((link) => link.setAttribute('target', '_blank'));
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.twitterHasLoaded !== this.state.twitterHasLoaded &&
            this.state.twitterHasLoaded &&
            this.rootRef.current
        ) {
            window.twttr.widgets.load(this.rootRef.current);
        }
    }

    componentWillUnmount() {
        this.unsubscribeOnLoad();
        window.removeEventListener('load', this.handleLoad);
    }

    getConfigurableCarousel(id) {
        if (!id) return null;

        const { configurableCarousels } = this.props.article;

        if (ArrayHelper.isEmpty(configurableCarousels)) return null;

        const foundConfigurableCarousels = configurableCarousels.find((configurableCarousel) => configurableCarousel.id === id);

        return foundConfigurableCarousels || null;
    }

    getArticleInfo = () => {
        const { heroMedia, url } = this.props.article;

        return {
            title: heroMedia.title,
            url: url || document.location.href,
        };
    };

    renderContent(content) {
        switch (content.contentType) {
            case contentTypes.TEXT:
                return <Richtext model={content} />;
            case contentTypes.QUOTE:
                return <Quote model={content} />;
            case contentTypes.IMAGE:
                return <Image model={content} />;
            case contentTypes.GALLERY:
                return <Gallery model={content} />;
            case contentTypes.VIDEO:
                return <Video
                    model={content}
                    gtm={this.getArticleInfo()}
                    bemList={content.title ? ['article', 'flexible'] : ['article']}
                />;
            // case contentTypes.PROMO:
            // return <Banner />;
            // case contentTypes.CLOUD_MATRIX:
            //     return <VideoCarousel
            //         endpointUrl={content.feedUrl}
            //         title={content.title}
            //         sponsor={content.sponsor}
            //         targetUrl={'/pro14tv/all-videos'}
            //     />;
            // case contentTypes.RELATED_CONTENT:
            //     configurableCarousel = this.getConfigurableCarousel(content.id);
            //
            //     if (!configurableCarousel) return null;
            //
            //     return <LatestNewsCarousel {...configurableCarousel} />;
            default:
                console.log('Unknown type:', content.contentType);
                return null;
        }
    }

    render() {
        const { article } = this.props;

        if (!article ||
            ArrayHelper.isEmpty(article.content) ||
            !this.state.twitterHasLoaded
        ) {
            return null;
        }

        const rootCss = generateBemCssModule('article-content', getBemList(this.props.bemList), styles);

        return (
            <div className={rootCss} ref={this.rootRef}>
                {article.content.map((content) => {
                    if (!content.contentType) return null;

                    const modName = content.contentType.toLowerCase().replace('_', '-');

                    const rowCss = generateBemCssModule('article-content__row', [modName], styles);
                    const containerCss = generateBemCssModule('article-content__container', [modName], styles);

                    return (
                        <div className={rowCss} key={content.id}>
                            <div className={containerCss}>
                                {this.renderContent(content)}
                            </div>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default ArticleContent;
