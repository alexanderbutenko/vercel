export const bemClasses = Object.freeze({
    DARK: 'dark',
    LIGHT: 'light',
    SIZE_MINI: 'size-mini',
    CLOSE_OUTSIDE: 'close-outside'
});

export const eventTypes = Object.freeze({
    OPEN: 'modal:open',
    CLOSE: 'modal:close',
});
