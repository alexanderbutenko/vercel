import React from 'react';
import { format, parseISO } from 'date-fns';
import { contentTypes } from 'fanx-ui-framework/general/app/enums';
import { DamImage } from 'fanx-ui-framework/components/dam-image';
import { Image } from 'fanx-ui-framework/components/image';
import { Video } from 'fanx-ui-framework/components/video';
import styles from '../scss/article-hero.module.scss';

const ArticleHero = ({ article }) => {
    const {
        author,
        displayCategory,
        readTimeMinutes,
        heroMedia,
        publishDate,
        singlePage,
    } = article;

    if (!heroMedia) return null;

    const getArticleInfo = () => ({
        title: heroMedia.content.title || heroMedia.title,
        url: article.url || document.location.href,
    });

    const renderContent = (content) => {
        switch (content.contentType) {
            case contentTypes.IMAGE:
                return <Image model={content} bemList={['flexible', 'no-title']} />;
            case contentTypes.VIDEO:
                return <Video
                    model={content}
                    gtm={getArticleInfo()}
                    bemList={['big', 'flexible']}
                />;
            default:
                console.log('Unknown type:', content.contentType);
                return null;
        }
    };

    return (
        <div className={styles['article-hero']}>
            {
                !singlePage &&
                <div className='container'>
                    <div className={styles['article-hero__wrap']}>
                        {
                            displayCategory &&
                            displayCategory.text &&
                            <div className={styles['article-hero__label']}>{displayCategory.text}</div>
                        }
                        <h1 className={styles['article-hero__title']}>{heroMedia.title}</h1>
                        <div className={styles['article-hero__bottom']}>
                            {
                                author &&
                                author.name &&
                                <>
                                    {
                                        author.imageUrl &&
                                        <div className={`${styles['article-hero__avatar']} of-cover`}>
                                            <DamImage image={author.imageUrl} />
                                        </div>
                                    }
                                    <span className={styles['article-hero__author']}>{author.name}</span>
                                </>
                            }
                            <span className={styles['article-hero__reading-time']}>{readTimeMinutes} min read</span>
                            {
                                publishDate &&
                                <time className={styles['article-hero__date']} dateTime={publishDate}>
                                    {format(parseISO(publishDate), 'd MMM yyyy')}
                                </time>
                            }
                        </div>
                    </div>
                </div>
            }
            <div className={`${styles['article-hero__media']} of-cover`}>
                {
                    heroMedia.content &&
                    renderContent(heroMedia.content)
                }
            </div>
        </div>
    );
};

export default ArticleHero;
