import React from 'react';
import styles from '../scss/article-preview.module.scss';

const ArticlePreview = ({ preview }) => {
    return (
        <div className={styles['article-preview']}>
            <div className={styles['article-preview__content']}>
                <div className="richtext">
                    <p>{preview}</p>
                </div>
            </div>
        </div>
    );
};

export default ArticlePreview;
