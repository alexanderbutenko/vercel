import cn from 'classnames/bind';
import React from 'react';
import Link from 'next/link';
import { ArrayHelper } from 'fanx-ui-framework/general/helpers';
import { getCategoryString } from 'fanx-ui-framework/general/helpers/helpers';
import styles from '../scss/article-tags.module.scss';

const ArticleTags = ({ childCss, categories }) => {
    if (ArrayHelper.isEmpty(categories)) return null;

    const cx = cn.bind(styles);

    return (
        <div className={cx(childCss, 'article-tags')}>
            <div className="container-tiny">
                <ul className={cx('article-tags__list')}>
                    {categories.map((category) => {
                        const formattedCategory = getCategoryString(category.text.toLocaleLowerCase());

                        return (
                            <li className={cx('article-tags__item')}
                                key={category.id}
                                onClick={() => {
                                }}
                            >
                                <Link
                                    href={'/latest/index'}
                                    as={`/latest/${formattedCategory}`}
                                >
                                    <a className={cx('article-tags__link')}>
                                        {category.text}
                                    </a>
                                </Link>
                            </li>
                        );
                    })}
                </ul>
            </div>
        </div>
    );
};

ArticleTags.defaultProps = {
    childCss: '',
};

export default ArticleTags;
