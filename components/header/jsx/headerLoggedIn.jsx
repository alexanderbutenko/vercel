import React from 'react';

import Link from 'next/link';
import { authentication } from 'fanx-ui-framework/general/services';
import s from '../scss/header.module.scss';

const HeaderLoggedIn = () => {
    const onLogoutClick = () => {
        authentication.logout();
    };
    return <>
        <div className="s.loggedOutWrap">
            <Link href={'/profile/'}>
                <a className={s.navLink}>Profile</a>
            </Link>
            <a className={s.navLink} onClick={onLogoutClick}>Logout</a>
        </div>
    </>;
};

export default HeaderLoggedIn;
