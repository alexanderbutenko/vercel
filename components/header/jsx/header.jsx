import React from 'react';
import Link from 'next/link';
import { authStore } from 'fanx-ui-framework/general/services';
import { useStore } from 'effector-react';
import NoSSR from 'react-no-ssr';
import s from '../scss/header.module.scss';
import HeaderLoggedOut from './headerLoggedOut';
import HeaderLoggedIn from './headerLoggedIn';

const Header = () => {
    const authState = useStore(authStore);
    return <header className={s.header}>
        <div className="container">
            <div className={s.inner}>
                <nav className={s.nav}>
                    <Link href={'/latest/'}>
                        <a className={s.navLink}>Latest</a>
                    </Link>
                </nav>
                <NoSSR onSSR={<HeaderLoggedOut />}>
                    <>
                        {!authState.isLoggedIn &&
                        <HeaderLoggedOut />
                        }

                        {authState.isLoggedIn &&
                        <HeaderLoggedIn />
                        }
                    </>
                </NoSSR>
            </div>
        </div>
    </header>;
};

export default Header;
