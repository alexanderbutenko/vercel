import React from 'react';
import Link from 'next/link';

import s from '../scss/header.module.scss';

const HeaderLoggedOut = () => {
    return <>
        <div className="s.loggedOutWrap">
            <Link href={'/profile/sign-in/'}>
                <a className={s.navLink}>Sign in</a>
            </Link>
            <Link href={'/profile/sign-up/'}>
                <a className={s.navLink}>Sign up</a>
            </Link>
        </div>
    </>;
};

export default HeaderLoggedOut;
