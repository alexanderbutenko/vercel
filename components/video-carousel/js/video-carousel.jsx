import React from 'react';
import { FetchService } from 'fanx-ui-framework/general/services';
import {
    mapCloudVideos,
    sortVideosByReleaseFrom,
    excludePaidVideos,
} from 'fanx-ui-framework/general/helpers/helpers';
import { BlockSpinner } from 'fanx-ui-framework/components/block-spinner';
import { Section } from 'fanx-ui-framework/components/section';
import { Carousel } from 'fanx-ui-framework/components/carousel';

class VideoCarousel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            videos: [],
            isFetching: false
        };
        this.fetchService = null;
    }

    componentDidMount() {
        this.fetchService = new FetchService({
            config: {
                url: this.props.endpointUrl
            },
            beforeFetch: () => { this.setState({ isFetching: false }); },
            successCallback: (data) => {
                this.setState({
                    videos: mapCloudVideos(data)
                        .filter(excludePaidVideos)
                        .sort(sortVideosByReleaseFrom),
                    isFetching: false
                });
            },
            errorCallback: () => {
                this.setState({ isFetching: false });
            }
        });

        this.fetchService.once();
    }

    componentWillUnmount() {
        if (this.fetchService) {
            this.fetchService.destroy();
        }
    }

    render() {
        if (!this.props.endpointUrl) return null;

        const {
            title,
            isTitleSmall,
            targetUrl,
            bemList,
            sponsor
        } = this.props;
        const { videos, isFetching } = this.state;

        if (isFetching) {
            return (
                <Section bemList={bemList}>
                    <BlockSpinner bemList={['light']} />
                </Section>
            );
        }

        if (!videos.length) return null;

        const topVideos = videos.slice(0, 10).map((video) =>
            // <StreamAmgCard
            //     key={video.id}
            //     video={mapCloudVideo(video)}
            //     playbackType={playbackTypes.POPUP}
            // />
            <>StreamAmgCard here</>);

        return (
            <Section bemList={bemList}>
                <Carousel
                    title={title}
                    slides={topVideos}
                    sponsor={sponsor}
                    targetUrl={videos.length > 10 ? targetUrl : null}
                    targetUrlText={'All videos'}
                    bemList={bemList}
                    isTitleSmall={isTitleSmall}
                    onIndexChanged={this.getButtonsState}
                />
            </Section>
        );
    }
}

VideoCarousel.defaultProps = {
    options: {}
};

export default VideoCarousel;
