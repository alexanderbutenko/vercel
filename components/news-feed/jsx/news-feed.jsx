import React, { useState } from 'react';
import { FetchService } from 'fanx-ui-framework/general/services';
import { BlockSpinner } from 'fanx-ui-framework/components/block-spinner';
import { Container } from 'fanx-ui-framework/components/container';
import { LatestNewsLayout } from 'fanx-ui-framework/components/latest-news-layout';
import { Endpoints } from 'fanx-ui-framework/general/app/endpoints';
import styles from '../scss/news-feed.module.scss';

const NewsFeed = (props) => {
    const [articles, setArticles] = useState(props.articles);
    const [metadata, setMetadata] = useState(props.metadata);
    const [qsParams] = useState(props.qsParams);
    const [isFetching, setIsFetching] = useState(false);

    const loadMore = () => {
        const config = {
            url: Endpoints.Articles,
            params: {
                ...qsParams,
                page: metadata.pageNumber + 1
            }
        };
        const latestNewsFetchService = new FetchService({
            config,
            beforeFetch: () => setIsFetching(true),
            successCallback: (data) => onSuccessFetch(data),
        });

        latestNewsFetchService.once();
    };

    const onSuccessFetch = (data) => {
        setArticles([...articles, ...data.data.articles]);
        setMetadata(data.metadata);
        setIsFetching(false);
    };

    return (
        <>
            <section className={styles.newsFeed}>
                <Container>
                    <LatestNewsLayout items={articles} />
                    {metadata.pageNumber < metadata.totalPages &&
                    <div className={styles.footer}>
                        <button
                            className="text-button text-button--icon text-button--border"
                            onClick={() => loadMore()}
                        >
                            Load More
                        </button>
                    </div>
                    }
                </Container>
            </section>
            {
                isFetching &&
                <BlockSpinner hasOverlay />
            }
        </>
    );
};

export default NewsFeed;
