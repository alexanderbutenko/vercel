import cn from 'classnames';
import React from 'react';
import Link from 'next/link';
import { format, parseISO } from 'date-fns';
import { generateModuleName, generateThumbnailSrc, getCategoryString } from 'fanx-ui-framework/general/helpers/helpers';
import { contentTypes } from 'fanx-ui-framework/general/app/enums';
import { DamImage } from 'fanx-ui-framework/components/dam-image';
import IconPlay from 'fanx-ui-framework/general/icons/iconPlay';
import styles from '../scss/index.module.scss';

export default class NewsCard extends React.Component {
    render() {
        const { article, childCss, bemList } = this.props;

        const {
            displayCategory,
            heroMedia,
            publishDate,
            author,
            slug
        } = article;

        const { content } = heroMedia;
        const typeMod = content?.contentType.toLowerCase();
        const category = getCategoryString(displayCategory.text);

        const bemlist = bemList || [];

        const bemCss = generateModuleName('newsCard', styles, [...bemlist, typeMod]);

        const getImageUrlByType = () => {
            if (!content) return null;

            if (content.contentType === contentTypes.VIDEO) {
                return generateThumbnailSrc(content.sourceSystemId);
                // return content.videoThumbnail;
            }

            return content.image;
        };

        return (
            <Link
                href={'/latest/[category]/[slug]'}
                as={`/latest/${category}/${slug}`}
            >
                <a className={cn(childCss, styles.newsCard, bemCss)}>
                    <div className={cn(styles.image, 'of-cover')}>
                        <DamImage image={getImageUrlByType()} />

                        <IconPlay className={styles.iconPlay} width={18} height={16} />

                        <div className={cn(styles.memberLogo, 'of-contain')}>
                            <DamImage image="c5fcf2d0-d489-4fa2-b8b4-1f267e871e29.png" />
                        </div>
                    </div>
                    <div className={styles.content}>
                        <div className={styles.topWrap}>
                            {
                                displayCategory &&
                                <p className={styles.label}>{displayCategory.text}</p>
                            }
                            {
                                publishDate &&
                                <time className={styles.date} dateTime={publishDate}>
                                    {format(parseISO(publishDate), 'd MMM yyyy')}
                                </time>
                            }
                        </div>
                        <h3 className={styles.title}>{heroMedia.title}</h3>
                        <div className={styles.bottom}>
                            {
                                author &&
                                author.name &&
                                <div className={styles.author}>
                                    {
                                        author.imageUrl &&
                                        <div className={cn(styles.avatar, 'of-cover')}>
                                            <DamImage image={author.imageUrl} />
                                        </div>
                                    }
                                    <span className={styles.authorName}>{author.name}</span>
                                </div>
                            }
                            {
                                article.readTimeMinutes > 0 &&
                                <div className={styles.readingTime}>
                                    {article.readTimeMinutes} min read
                                </div>
                            }
                            {
                                publishDate &&
                                <time className={cn(styles.date, styles.dateBottom)}>
                                    dateTime={publishDate}
                                    {format(parseISO(publishDate), 'd MMM yyyy')}
                                </time>
                            }
                            <div
                                className={cn(styles.memberLogo, 'of-contain')}>
                                <DamImage image="c5fcf2d0-d489-4fa2-b8b4-1f267e871e29.png" />
                            </div>
                        </div>
                    </div>
                </a>
            </Link>
        );
    }
}
