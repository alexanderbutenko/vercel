export const playerTypes = Object.freeze({
    EMBED: 'embed',
    THUMBNAIL: 'thumbnail',
});

export const playbackTypes = Object.freeze({
    INLINE: 'inline',
    POPUP: 'popup'
});

export const eventTypes = Object.freeze({
    PLAY: 'play'
});
