import React from 'react';
import PropTypes from 'prop-types';

class CarouselButtons extends React.Component {
    render() {
        return (
            <div className="slider-controls" data-glide-el="controls">
                <button
                    className="carousel-button carousel-button--prev slider-back"
                    type="button"
                    data-glide-dir="<"
                    onClick={() => this.props.onClickPrevBtn('Prev')}
                >
                    <span className="visually-hidden">Prev</span>
                    <svg className="carousel-button__icon icon" width="8" height="10" viewBox="0 0 8 10">
                        <path d="M0 5l8 5V0z" />
                    </svg>
                </button>
                <button
                    className="carousel-button carousel-button--next slider-next"
                    type="button"
                    data-glide-dir=">"
                    onClick={() => this.props.onClickNextBtn('Next')}
                >
                    <span className="visually-hidden">Next</span>
                    <svg className="carousel-button__icon icon" width="8" height="10" viewBox="0 0 8 10">
                        <path d="M8 5l-8 5V0z" />
                    </svg>
                </button>
            </div>
        );
    }
}

CarouselButtons.defaultProps = {
    onClickPrevBtn: () => {},
    onClickNextBtn: () => {},
};

CarouselButtons.propTypes = {
    onClickPrevBtn: PropTypes.func,
    onClickNextBtn: PropTypes.func,
};

export default CarouselButtons;
