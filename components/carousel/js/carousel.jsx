import React from 'react';
import Glide from '@glidejs/glide';
import { ArrayHelper } from 'fanx-ui-framework/general/helpers';
import { generateBemCss } from 'fanx-ui-framework/general/helpers/helpers';
// import app from 'fanx-ui-framework/general/js/app';
// import { gtmService } from 'lib/project/services';
import { ArrowDisabler } from 'fanx-ui-framework/components/glide';
import { eventBus } from 'fanx-ui-framework/components/event-bus';
import { eventTypes as modalEventTypes } from 'fanx-ui-framework/components/modal';
import { DamImage } from 'fanx-ui-framework/components/dam-image';
import { CarouselButtons } from 'fanx-ui-framework/components/carousel-buttons';
import { CarouselDots } from 'fanx-ui-framework/components/carousel-dots';

class Carousel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fakeDOts: null,
        };
        this.glideOptions = {
            perView: 3,
            rewind: false,
            bound: true,
            gap: 26,
            breakpoints: {
                768: {
                    perView: 1,
                    gap: 14
                }
            }
        };
        this.carousel = null;
        this.rootRef = React.createRef();
        this.cardRefs = new Map();
    }

    componentDidMount() {
        // fix rendering in modal
        this.initCarousel();
        eventBus.addListener(modalEventTypes.OPEN, this.onModalOpen);
    }

    componentDidUpdate(prevProps) {
        const prevKey = this.getCarouselKey(prevProps.slides);
        const key = this.getCarouselKey(this.props.slides);

        if (key && prevKey !== key) {
            this.destroyCarousel();
            this.initCarousel();
        }
    }

    componentWillUnmount() {
        this.destroyCarousel();
        eventBus.removeListener(modalEventTypes.OPEN, this.onModalOpen);
    }

    initCarousel() {
        this.carousel = new Glide(this.rootRef.current, {
            ...this.glideOptions,
        });

        this.carousel.on('run.before', () => {
            this.cardRefs.forEach((el) => {
                el.classList.add('is-active');
            });
        });

        this.carousel.on('run.after', () => {
            this.setActiveSlides(this.carousel.index, this.carousel.settings.perView);
            this.updateFakeDots(this.carousel.index);
        });

        this.carousel.on('mount.after', () => {
            this.setActiveSlides(this.carousel.index, this.carousel.settings.perView);
        });

        this.carousel.mount({ ArrowDisabler });
        this.updateFakeDots(0);
    }

    destroyCarousel() {
        if (this.carousel) {
            this.glideOptions.startAt = this.carousel.index;
            this.carousel.destroy();
        }
    }

    getCarouselKey(slides) {
        if (ArrayHelper.isEmpty(slides)) return '';

        return slides.map((slide) => slide.id).join('-');
    }

    onModalOpen = () => {
        this.destroyCarousel();
        this.initCarousel();
    };

    updateFakeDots(currentIndex) {
        this.setState({
            fakeDots: {
                isFirst: currentIndex === 0,
                isLast: currentIndex > (this.props.slides.length - 1) - this.carousel.settings.perView,
            }
        });
    }

    setActiveSlides(currentIndex, perView) {
        this.cardRefs.forEach((el, i) => {
            if (i >= currentIndex && i < currentIndex + perView) {
                el.classList.add('is-active');
            } else {
                el.classList.remove('is-active');
            }
        });
    }

    onClick = () => {
        // gtmService.push(app.state.siteSection, 'Related', `All related > ${document.title}`);
    };

    onClickCarouselBtn = (label) => {
        // gtmService.push(app.state.siteSection, 'Next / Prev', `${label} > ${document.location.href}`);
    };

    render() {
        const {
            bemList,
            slides,
            targetUrlText,
            targetUrl,
            sponsor
        } = this.props;

        const rootCss = generateBemCss('carousel', bemList);
        const hasmoreThan3 = slides.length > 3;

        return (
            <div className={rootCss}>
                <div className="glide glide--base" ref={this.rootRef}>
                    <div className="carousel__header">
                        {
                            sponsor &&
                            sponsor.imageUrl &&
                            <a
                                className="carousel__partner"
                                href={sponsor.linkUrl}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <span>Presented by</span>
                                <div className="carousel__partner-logo of-contain">
                                    <DamImage image={sponsor.imageUrl} alt={'Sponsor Logo'} />
                                </div>
                            </a>
                        }

                        {
                            hasmoreThan3 &&
                        <div className="carousel__controls">
                            <CarouselButtons
                                onClickPrevBtn={this.onClickCarouselBtn}
                                onClickNextBtn={this.onClickCarouselBtn}
                            />
                        </div>
                        }
                    </div>
                    <div className="carousel__track glide__track" data-glide-el="track">
                        <ul className="carousel__slides glide__slides">
                            {
                                slides.map((slide, i) =>
                                    <li
                                        className="carousel__slide glide__slide"
                                        key={i}
                                        ref={(el) => {
                                            this.cardRefs.set(i, el);
                                        }}
                                    >{slide}</li>)
                            }
                        </ul>
                    </div>
                    {
                        <div className="carousel__bottom">
                            {
                                targetUrl &&
                                <a
                                    className="carousel__button text-button text-button--border"
                                    href={targetUrl}
                                    onClick={this.onClick}
                                >
                                    <span>{targetUrlText}</span>
                                </a>
                            }
                            {
                                hasmoreThan3 &&
                            <div className="carousel__dots">
                                <CarouselDots
                                    fakeDots={this.state.fakeDots}
                                    bemList={bemList}
                                />
                            </div>
                            }
                        </div>
                    }
                </div>
            </div>
        );
    }
}

export default Carousel;
