import React from 'react';
import { Endpoints } from 'fanx-ui-framework/general/app/endpoints';
import { getSrcSetStreamAmg } from 'fanx-ui-framework/general/helpers/helpers';
import { sizes } from 'fanx-ui-framework/general/app/enums';
import { LazyImage } from 'fanx-ui-framework/components/lazy-image';

const DamImage = ({ image, ...rest }) => {
    if (!image) return null;

    if (image.includes('streamamg')) {
        return <LazyImage image={getSrcSetStreamAmg(image)} {...rest} />;
    }

    if (image.includes('wp-content')) {
        return <LazyImage image={image} {...rest} />;
    }

    let path = `${Endpoints.IncrowdSports}${image}`;

    if (image.includes('http')) {
        path = image;
    }

    const srcString = [];

    const getDelimiter = () => {
        if (image.includes('?')) return '&';

        return '?';
    };

    sizes.forEach((size) => {
        srcString.push(`${path}${getDelimiter()}width=${size} ${size}w`);
    });

    return <LazyImage image={srcString.join(', ')} {...rest} />;
};

export default DamImage;
