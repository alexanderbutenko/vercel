import React, { useState } from 'react';
import formStyles from 'fanx-ui-framework/components/forms/scss/forms.module.scss';
import { authentication } from 'fanx-ui-framework/general/services';

import { handleFormResponse } from 'fanx-ui-framework/components/forms/js/form-helpers';

const SigninForm = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [formErrorMessage, setFormErrorMessage] = useState('');
    const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        authentication
            .login({ username, password })
            .then((response) => {
                handleFormResponse(response, setFormErrorMessage);
            })
            .catch((error) => {
                console.log('login error', error);
            });
    };

    return (
        <>
            <form onSubmit={submitHandler}>
                <div className={formStyles.formRow}>
                    <label className={formStyles.label} htmlFor="username">
                        Username
                    </label>
                    <input
                        id="username"
                        required={true}
                        value={username}
                        onChange={(e) => setUsername(e.target.value)}
                    />
                </div>
                <div className={formStyles.formRow}>
                    <label className={formStyles.label} htmlFor="password">
                        Password
                    </label>
                    <input
                        id="password"
                        value={password}
                        required={true}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <button type="submit" className="initial-mt-20">
                    Submit
                </button>
                {formErrorMessage && <div className={formStyles.formError}>{formErrorMessage}</div>}
            </form>
        </>
    );
};

export default SigninForm;
