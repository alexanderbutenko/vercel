import React, { useEffect, ReactElement } from 'react';
import { useStore } from 'effector-react';
import { authStore } from 'fanx-ui-framework/general/services';
import { useRouter } from 'next/router';

interface IProfileRedirectWrapProps {
    children?: ReactElement | ReactElement[];
}

export const ProfileRedirectWrap = (props: IProfileRedirectWrapProps): JSX.Element => {
    const authState = useStore(authStore);
    const router = useRouter();

    useEffect(() => {
        if (authState.isLoggedIn) {
            router.push('/profile');
        }
    }, [authState]);
    return <>{props.children}</>;
};
