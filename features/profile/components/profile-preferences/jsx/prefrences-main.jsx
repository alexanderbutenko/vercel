import React, { useState, useEffect } from 'react';
import { authentication } from 'fanx-ui-framework/general/services';

const converMarketingPreferencesStructure = (preferencesArr) => {
    const result = preferencesArr.map((item) => ({
        key: item.key,
        text: item.text,
        preferences: {
            email: item.preferences.email,
            sms: item.preferences.sms,
            post: item.preferences.post,
            phone: item.preferences.phone,

        }

    }));

    result.sort((a, b) => {
        if (a.key > b.key) {
            return 1;
        }
        if (a.key < b.key) {
            return -1;
        }
        return 0;
    });

    return result;
};

export const PreferencesMain = () => {
    const [marketingPreferences, setMarketingPreferences] = useState(null);

    const submitHandler = (e) => {
        e.preventDefault();

        authentication.updateMarketingPreferences(marketingPreferences).then((result) => {
            if (result.success) {
                setMarketingPreferences(converMarketingPreferencesStructure(result.payload.preferences));
                alert('success');
            } else {
                console.log(result);
                alert('something went wrong');
            }
        });
    };

    const handleChange = (e, key, name) => {
        const mp = JSON.parse(JSON.stringify(marketingPreferences));
        mp.forEach((item) => {
            if (item.key === key) {
                item.preferences[name] = e.target.checked;
            }
        });
        setMarketingPreferences(mp);
    };

    useEffect(() => {
        authentication.getMarketingPreferences().then((preferencesResult) => {
            if (preferencesResult.success) {
                const mp = converMarketingPreferencesStructure(preferencesResult.payload.data);
                setMarketingPreferences(mp);
            } else {
                setMarketingPreferences(null);
            }
        });
    }, []);

    return (
        <>
            {marketingPreferences &&
            <>
                <form onSubmit={submitHandler} className='initial-mt-20'>
                    {marketingPreferences.map((item) =>
                        <div className='initial-mb-20' key={item.key}>
                            {item.key}
                            <h4>{item.text}</h4>
                            <div className='initial-mt-10'>
                                <label>
                                    <input type='checkbox' onChange={(e) => handleChange(e, item.key, 'email')} checked={item.preferences.email} />
                                Email
                                </label>
                            </div>
                            <div className='initial-mt-10'>
                                <label>
                                    <input type='checkbox' onChange={(e) => handleChange(e, item.key, 'sms')} checked={item.preferences.sms} />
                                SMS
                                </label>
                            </div>
                            <div className='initial-mt-10'>
                                <label>
                                    <input type='checkbox' onChange={(e) => handleChange(e, item.key, 'phone')} checked={item.preferences.phone} />
                                    Phone
                                </label>
                            </div>
                        </div>)}

                    <button type='submit'>Save</button>
                </form>
            </>
            }

        </>
    );
};
