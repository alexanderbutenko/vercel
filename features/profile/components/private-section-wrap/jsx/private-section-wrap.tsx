import React, { useEffect, ReactElement } from 'react';
import { useStore } from 'effector-react';
import { authStore } from 'fanx-ui-framework/general/services';
import { useRouter } from 'next/router';

interface IPrivateSectionWrapProps {
    children?: ReactElement | ReactElement[];
}

export const PrivateSectionWrap = (props: IPrivateSectionWrapProps): JSX.Element => {
    const authState = useStore(authStore);
    const router = useRouter();

    useEffect(() => {
        console.log(authState);
        if (!authState.isLoggedIn) {
            router.push('/profile/sign-in');
        }
    }, [authState]);
    return <>{props.children}</>;
};
