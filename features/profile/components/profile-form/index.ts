import { ProfileMain } from './jsx/profile-main';
import { ProfileSide } from './jsx/profile-side';

export { ProfileMain, ProfileSide };
