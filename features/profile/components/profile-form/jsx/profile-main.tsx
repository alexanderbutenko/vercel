import React, { useState, useEffect, FC } from 'react';
import { authentication, authStore } from 'fanx-ui-framework/general/services';
import { useStore } from 'effector-react';
import {
    IProfile,
    IProfileResponse,
} from 'fanx-ui-framework/general/services/js/authentication/authentication';
import { IGeneralFormResponse } from 'fanx-ui-framework/general/interfaces/form-interfaces';
import { ProfileView } from './profile-view';
import { ProfileEdit } from './profile-edit';

const profileScreens = {
    VIEW: 'VIEW',
    EDIT: 'edit',
};

export const ProfileMain: FC = () => {
    const [profile, setProfile] = useState<IProfile | null | undefined>(null);
    const [screen, setScreen] = useState(profileScreens.VIEW);
    const authState = useStore(authStore);

    const goToEditScreen = (): void => {
        setScreen(profileScreens.EDIT);
    };

    const goToViewScreen = () => {
        setScreen(profileScreens.VIEW);
    };

    const onProfileUpdated = (profileResult: IProfileResponse) => {
        setProfile(profileResult.payload);
    };

    useEffect(() => {
        authentication.getProfile().then((profileResult: IProfileResponse) => {
            console.log(profileResult);
            if (profileResult.success) {
                setProfile(profileResult.payload);
            } else {
                setProfile(null);
            }
        });
    }, [authState]);
    return (
        <>
            {profile && (
                <>
                    {screen === profileScreens.VIEW && (
                        <ProfileView profile={profile} goToEditScreen={goToEditScreen} />
                    )}

                    {screen === profileScreens.EDIT && (
                        <ProfileEdit
                            profile={profile}
                            goToViewScreen={goToViewScreen}
                            onProfileUpdated={onProfileUpdated}
                        />
                    )}
                </>
            )}
        </>
    );
};
