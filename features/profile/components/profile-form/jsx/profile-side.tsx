import React, { FC } from 'react';
import { ActiveLink } from 'fanx-ui-framework/components/active-link';
import s from '../scss/profile-side.module.scss';

export const ProfileSide: FC = () => (
    <>
        <ActiveLink activeClassName={s.active} href="/profile">
            <a className={s.link}>Profile</a>
        </ActiveLink>
        <ActiveLink activeClassName={s.active} href="/profile/preferences">
            <a className={s.link}>Preferences</a>
        </ActiveLink>
    </>
);
