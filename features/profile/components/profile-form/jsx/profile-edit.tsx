import React, { useState } from 'react';
import formStyles from 'fanx-ui-framework/components/forms/scss/forms.module.scss';
import { authentication } from 'fanx-ui-framework/general/services';
import {
    IProfile,
    IProfileParameters,
    IProfileResponse,
} from 'fanx-ui-framework/general/services/js/authentication/authentication';

interface IProfileEditProps {
    profile: IProfile;
    goToViewScreen(): void;
    onProfileUpdated(resp: IProfileResponse): void;
}

export const ProfileEdit = ({ profile, goToViewScreen, onProfileUpdated }: IProfileEditProps) => {
    const [formErrorMessage, setFormErrorMessage] = useState('');

    const submitHandler = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formDataObj = Object.fromEntries(
            new FormData(e.target as HTMLFormElement)
        ) as IProfileParameters;
        authentication.updateProfile(formDataObj).then((profileResult) => {
            if (profileResult.success) {
                goToViewScreen();
                onProfileUpdated(profileResult);
            } else {
                setFormErrorMessage(profileResult.message || '');
            }
        });
    };
    return (
        <>
            <form onSubmit={submitHandler}>
                <div className={formStyles.formRow}>
                    <label className={formStyles.label} htmlFor="firstName">
                        First Name
                    </label>
                    <input id="firstName" name="firstName" defaultValue={profile.firstName || ''} />
                </div>

                <div className={formStyles.formRow}>
                    <label className={formStyles.label} htmlFor="username">
                        Last Name
                    </label>
                    <input id="lastName" name="lastName" defaultValue={profile.lastName || ''} />
                </div>

                <div className="initial-mt-20">
                    <button type="button" onClick={goToViewScreen}>
                        Cancel
                    </button>
                    <button type="submit">Submit</button>
                </div>

                {formErrorMessage && <div className={formStyles.formError}>{formErrorMessage}</div>}
            </form>
        </>
    );
};
