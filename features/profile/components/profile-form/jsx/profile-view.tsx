import React from 'react';
import s from 'fanx-ui-framework/shared/styles/labeled-data.module.scss';
import { IProfile } from 'fanx-ui-framework/general/services/js/authentication/authentication';

interface IProfileViewProps {
    profile: IProfile;
    goToEditScreen(): void;
}

export const ProfileView = ({ profile, goToEditScreen }: IProfileViewProps): JSX.Element => (
    <>
        <div className={s.row}>
            <div className={s.label}>Email</div>
            <div>{profile.email}</div>
        </div>
        {profile.firstName && (
            <div className={s.row}>
                <div className={s.label}>First Name</div>
                <div>{profile.firstName}</div>
            </div>
        )}

        {profile.lastName && (
            <div className={s.row}>
                <div className={s.label}>Last Name</div>
                <div>{profile.lastName}</div>
            </div>
        )}

        <div className="initial-mt-30">
            <button type="button" onClick={goToEditScreen}>
                Edit profile
            </button>
        </div>
    </>
);
